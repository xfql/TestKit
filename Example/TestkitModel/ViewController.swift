//
//  ViewController.swift
//  TestkitModel
//
//  Created by Guoxiafei on 12/19/2019.
//  Copyright (c) 2019 Guoxiafei. All rights reserved.
//

import UIKit
import TestkitModel

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let first = FirstModel()
        first.logFirst()

        let second = SecondModel()
        second.logSecond()

//        let one = One()
//        one.logOne()
        let firsrV = FirstView(frame: CGRect(x: 10, y: 100, width: 400, height: 400))
        view.addSubview(firsrV)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

