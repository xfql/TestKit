//
/**
* @Name: FirstView.swift
* @Description:
* @Author: guoxiafei
* @Date: 2019/12/20
* @Copyright: 
*/


import UIKit

public class FirstView: UIView {

    var imageView : UIImageView!

    var firXibv : FirstXibView!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override public init(frame: CGRect) {
        super.init(frame: frame)

        imageView = UIImageView(frame: CGRect(x: 10, y: 10, width: 100, height: 100))
        imageView.image = UIImage.imageWithNameFromBundle("编组 43", "First", targetClass: FirstView.self)
        addSubview(imageView)

        firXibv = FirstXibView.loadFromNib("First", FirstXibView.self)
            //UINib.loadNibWithNameFromBundle("FirstXibView", "First", targetClass: FirstXibView.self, owner: self)
            //UINib(nibName: "FirstXibView", bundle: Bundle.subBundleWithBundleNameAndClass("First", targetClass: FirstView.self)).instantiate(withOwner: self, options: nil).last as? FirstXibView

        addSubview(firXibv)

        firXibv.frame = CGRect(x: 10, y: 110, width: 200, height: 200)
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


}



public extension Bundle {
    class func subBundleWithBundleNameAndClass(_ bundleName: String, targetClass : AnyClass) -> Bundle? {
        let bundlePath = Bundle(for: targetClass).resourcePath?.appending("/\(bundleName).bundle")
        return (bundlePath != nil) ? Bundle(path: bundlePath!) : nil
    }
}

// 协议
protocol NibLoadable {
    // 具体实现写到extension内
}

extension NibLoadable where Self : UIView {
    static func loadFromNib(_ bundleName: String,_ targetClass : AnyClass,_ nibname : String? = nil) -> Self {
        let bundle = Bundle.subBundleWithBundleNameAndClass(bundleName, targetClass: targetClass)
        let loadName = nibname == nil ? "\(self)" : nibname!
        let xibview = bundle?.loadNibNamed(loadName, owner: nil, options: nil)?.first as! Self
            //UINib(nibName: nibName, bundle: bundle).instantiate(withOwner: self, options: nil).last as? T
        return xibview
//        return Bundle.main.loadNibNamed(loadName, owner: nil, options: nil)?.first as! Self
    }
}

public extension UINib {
    class func loadNibWithNameFromBundle<T>(_ nibName: String, _ bundleName: String, targetClass : T.Type, owner: Any?) -> T? {
        let bundle = Bundle.subBundleWithBundleNameAndClass(bundleName, targetClass: targetClass as! AnyClass)
        let xibview = bundle?.loadNibNamed(nibName, owner: nil, options: nil)?.first as? T
            //UINib(nibName: nibName, bundle: bundle).instantiate(withOwner: self, options: nil).last as? T
        return xibview
    }
}

public extension UIImage{
    class func imageWithNameFromBundle(_ imageName: String, _ bundleName: String, targetClass : AnyClass) -> UIImage? {
        let bundle = Bundle.subBundleWithBundleNameAndClass(bundleName, targetClass: targetClass)
        return UIImage(named: imageName, in: bundle, compatibleWith: nil)
    }
}
