//
/**
* @Name: FirstModel.swift
* @Description:
* @Author: guoxiafei
* @Date: 2019/12/19
* @Copyright: 
*/


import UIKit

public class FirstModel: NSObject {
    public func logFirst() {
        print("logFirst")
    }
}
