//
/**
* @Name: SecondModel.swift
* @Description:
* @Author: guoxiafei
* @Date: 2019/12/19
* @Copyright: 
*/


import UIKit

open class SecondModel: NSObject {
    open func logSecond() {
        print("logSecond")
    }
}
