#
# Be sure to run `pod lib lint TestkitModel.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'TestkitModel'
  s.version          = '0.0.5'
  s.summary          = 'A short description of TestkitModel.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
    0.0.4 新增加 图片和Xib的加载方式,添加资源库
                       DESC

  s.homepage         = 'https://gitee.com/xfql/TestKit'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Guoxiafei' => 'guoxiafei1991@163.com' }
  s.source           = { :git => 'https://gitee.com/xfql/TestKit.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'
  s.requires_arc = true
  s.ios.deployment_target = '8.0'

#  s.source_files = 'TestkitModel/Classes/**/*'

  s.subspec 'Component' do |ss|

      ss.subspec 'First' do |sss|
      sss.source_files         = 'TestkitModel/Component/First/*'
       sss.resource_bundles = {
         'First' => ['TestkitModel/ComponentBundle/FirstBundle/*.{xcassets,xib}']
       }
#      sss.resources = 'TestkitModel/ComponentBundle/FirstBundle/*.png,*xib'
    end

      ss.subspec 'Second' do |sss|
        sss.source_files         = 'TestkitModel/Component/Second/*'
        sss.resource_bundles = {
          'Second' => ['TestkitModel/ComponentBundle/SecondBundle/*.xcassets']
        }
      end
  end
  # s.resource_bundles = {
  #   'TestkitModel' => ['TestkitModel/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
