echo "-------------START---------------"
echo "------------------------------------------------------------------------------------"
pwd
cd ./Example
pod install
cd ..
pwd
echo "------------------------------------------------------------------------------------"
pod lib lint --no-clean --use-libraries --allow-warnings TestkitModel.podspec  --verbose
git tag 0.0.5
git add .
git commit -am "新增加 图片和Xib的加载方式,添加资源库"
git pull
git push
git push --tag
pod repo add TestkitModel https://gitee.com/xfql/TestKit.git
pod repo push --allow-warnings TestkitModel TestkitModel.podspec
#pod spec lint TestkitModel.podspec --verbose
pod trunk --allow-warnings push TestkitModel.podspec
echo "-----------------------------------------END-------------------------------------------"
